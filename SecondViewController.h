//
//  SecondViewController.h
//  LoginPage
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/19.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *showNameLabel;

@property (strong,nonatomic) NSString *name;

- (IBAction)done:(id)sender;

@end
