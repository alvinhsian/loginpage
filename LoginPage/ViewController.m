//
//  ViewController.m
//  LoginPage
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/18.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//傳遞資料到下一個畫面（記得先匯入第二個畫面控制器的標頭檔）
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
/*
    以下這段是舊版Xcode自動產生的template時一起自動被產生。
    在Xcode6之後不需要了
 */
//    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
//        [[segue destinationViewController] setDelegate:self];
//    }
    if ([[segue identifier] isEqualToString:@"LoginSegue"]){
    
        //接收第一個畫面傳過來的參數
        SecondViewController *controller = segue.destinationViewController;
        //設定第二個畫面的NString的文字為第一個畫面帳號的文字
        controller.name = _nameTextField.text;
        
    }
    
    
}

- (IBAction)loginButton:(id)sender {

    if ([_nameTextField.text isEqualToString:@"alvin@gmail.com"]&&[_passwordTextField.text isEqualToString:@"zxc123"]) {
        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
    }
    
}

//Copy from UITextFieldDelegate
//實作觸碰帳號或密碼欄位時彈出虛擬鍵盤
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_nameTextField) {
        [_passwordTextField becomeFirstResponder];
    }else if(textField==_passwordTextField){
        //鍵盤收掉
        [_passwordTextField resignFirstResponder];
    }
    return YES;
}

@end
