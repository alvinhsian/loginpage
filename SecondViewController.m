//
//  SecondViewController.m
//  LoginPage
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/19.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //第二個畫面將資料顯示到Label
//    _showNameLabel.text = _name;
    
    //將email改為只顯示@之前的名字
    NSRange range = [_name rangeOfString:@"@"]; //指定被切割字串的參考點
    
    if (range.location != NSNotFound) {  //如果該參考點存在的話
        //取出參考點之前（左邊）的字串
        NSString *nameOnly = [_name substringToIndex:range.location];
        
        /* 
         * NSString類別裡，總共有這三種字串切割法
         * -(NSString *)substringFromIndex:(NSInteger)anIndex
         *  根據參考點取得右邊字串（不包括參考點）
         *
         * -(NSString *)substringToIndex:(NSInteger)anIndex
         *  根據參考點取得左邊字串（不包括參考點）
         *
         * -(NSString *)substringWithRange:(NSRange)aRange
         *  根據參考點取得右邊指定長度的字串 
         *  NSRange *range = NSMakeRange(7,3)
         */
        
        
        //將 nameOnly 改為首字母大寫
        _showNameLabel.text = [nameOnly capitalizedString];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)done:(id)sender {
    //按下Done之後做的事
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
